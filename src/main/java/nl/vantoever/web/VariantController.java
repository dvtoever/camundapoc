package nl.vantoever.web;

import nl.vantoever.service.CamundaService;
import nl.vantoever.web.dto.VariantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Endpoint om varianten op te vragen via de rule engine
 */
@Component
@Path("/")
@Produces("application/json")
public class VariantController {

    @Autowired
    private CamundaService camundaService;

    @GET
    @Path("/varianten")
    public VariantResponse getVarianten() {
        camundaService.runProcessByKey("bepalenVariant");

        final VariantResponse variantResponse = new VariantResponse();
        variantResponse.addVariant("variant1");
        variantResponse.addVariant("variant2");

        return variantResponse;
    }
}
