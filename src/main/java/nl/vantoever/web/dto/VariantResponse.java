package nl.vantoever.web.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dvtoever on 13-12-2015.
 */
public class VariantResponse {

    private List<String> varianten = new ArrayList<>();

    public void addVariant(String variantNaam) {
        this.varianten.add(variantNaam);
    }

    public List getVarianten() {
        return varianten;
    }

    public void setVarianten(List varianten) {
        this.varianten = varianten;
    }
}
