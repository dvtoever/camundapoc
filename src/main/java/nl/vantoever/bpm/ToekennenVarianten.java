package nl.vantoever.bpm;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * Camunda delegatie die wordt aangeroepen vanuit het BepalenVarianten proces
 */
public class ToekennenVarianten implements JavaDelegate {

    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("Toekennen varianten proces aangeroepen!!");
    }
}
