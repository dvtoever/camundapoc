package nl.vantoever.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by dvtoever on 12-12-2015.
 */
@EnableAutoConfiguration
@Configuration
@ImportResource(value = "classpath:/applicationContext.xml")
@ComponentScan(basePackages = "nl.vantoever")
public class CamundaConfiguration {


}

