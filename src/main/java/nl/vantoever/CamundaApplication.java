package nl.vantoever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class CamundaApplication extends SpringBootServletInitializer {



    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CamundaApplication.class);
    }

    public static void main(String[] args) {
        new CamundaApplication()
                .configure(new SpringApplicationBuilder(CamundaApplication.class))
                .run(args);
    }
}
