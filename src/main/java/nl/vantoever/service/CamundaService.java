package nl.vantoever.service;

import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Opstarten van de Camunda rule engine
 */
@Service
public class CamundaService {

    @Autowired
    private RuntimeService runtimeService;

    public void runProcessByKey(String key) {
        runtimeService.startProcessInstanceByKey(key);
    }
}
